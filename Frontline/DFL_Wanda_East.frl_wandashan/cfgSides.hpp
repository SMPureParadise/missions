class cfgFactions {
    class West {
        factionClass = "RHS_USMC_W";
    };

    class East : West {
        factionClass = "RHS_RUA_W";
    };
};
