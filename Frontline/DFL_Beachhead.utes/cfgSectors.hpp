class CfgSectors {
    class base_west {
        designator = "Beach landing"; // Name in spawn list
        sectortype = "mainbase"; // Mainbase to allow spawning
        aiSpawnAllow = 0; // 0 to prevent AI spawning here
        aiSpawnOnly = 0; // 1 to only allow AI spawning here
        //spawnMarker = "baseSpawn_west_1"; // Different marker than  middle
        //spawnHeight = 15; // Height above sea/terrain
    };

	class base_west_1 {
        designator = "Carrier";
        sectortype = "mainbase";
        spawnMarker = "baseSpawn_west_1";
        spawnHeight = 15.86;
        aiSpawnAllow = 0;
    };

	class base_west_2 {
        designator = "";
        sectortype = "mainbase";
        aiSpawnAllow = 1;
        aiSpawnOnly = 1;
        spawnMarker = "baseSpawn_west_2";
    };

    class base_east {
        designator = "Beach Landing";
        sectortype = "mainbase";
        aiSpawnAllow = 0;
    };

   class base_east_1 {
        designator = "";
        sectortype = "mainbase";
        aiSpawnAllow = 1;
        aiSpawnOnly = 1;
        spawnMarker = "baseSpawn_east_1";
    };
};
