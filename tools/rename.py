import os
import sys

directory = os.path.dirname(os.path.dirname(os.path.realpath(sys.argv[0]))) #get the directory of your script
missionspath = os.path.join(directory, "Frontline")
for subdir, dirs, files in os.walk(missionspath):
 for filename in files:
  if filename == 'cfgWeather123.hpp':
   subdirectoryPath = os.path.join(missionspath, subdir) #get the path to your subdirectory
   print("Updating {}".format(subdirectoryPath))
   newFilePath = os.path.join(subdirectoryPath, "cfgWeather.hpp") #get the path to your file
   filepath = os.path.join(subdirectoryPath, filename) #get the path to your file
   os.rename(filepath, newFilePath) #rename your file
