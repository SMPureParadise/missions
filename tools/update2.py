#!/usr/bin/env python3

import os
import sys
import shutil
import fileinput

from distutils.dir_util import copy_tree

##########################
SQUADSETS = {
    'USA': "RHS_USMC_Woodland",
    'RU': "RHS_RUA_Woodland",
    'NATO': "Apex_NATO",
    'PRC': "Apex_China",
    'Wehrmacht': "IFA_Wehr",
    'Red Army': "IFA_USSR",
    'U.S. Army': "IFA_USA",
    'INS': "RHS_Insurgents_Desert"
}

# This will add the Squads entry to missions that don't have it yet
def update_file(filepath):
    with open(filepath, "r") as fileread:
        data = fileread.read()


    lines = data.splitlines()
    firstline = lines[0]
    print('Firstline: {}'.format(firstline))
    if not (firstline == "#include \"metadata.hpp\""):
        print('Adding the line')
        with open(filepath, 'w') as modified: modified.write("#include \"metadata.hpp\"\n" + data)


def missions_walk(missionspath, copypath):
    os.chdir(missionspath)
    for o_value in os.listdir(missionspath):
        if o_value == "Archive":
            continue

        subpath = os.path.join(missionspath, o_value)
        print("subpath: {}".format(subpath))

        for p_value in os.listdir(subpath):
            path = os.path.join(missionspath, subpath, p_value)
            if not os.path.isdir(path):
                continue
            if p_value.startswith("."):
                continue
            if p_value.startswith("FL_"):
                continue

            missionfile = os.path.join(path, "mission.sqm")
            if not os.path.isfile(missionfile):
                continue

            print("# Updating {} ...".format(p_value))

            copy_tree(copypath, path)
            update_file(os.path.join(path, "description.ext"))


def update_missions():
    print("""
  ###################
  # Frontline Build #
  ###################
""")


    scriptpath = os.path.realpath(__file__)
    projectpath = os.path.dirname(os.path.dirname(scriptpath))
    copypath = os.path.join(projectpath, "tools", "missionupdate")
    missionspath = os.path.join(projectpath, "Frontline")

    print("# Updating {} ...".format(missionspath))
    print("# Updating {} ...".format(copypath))
    print("###################")

    missions_walk(missionspath, copypath)

if __name__ == "__main__":
    sys.exit(update_missions())
